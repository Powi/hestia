#!/usr/local/bin/python3.6

#Command line parameter:
#1: Discord TOKEN

import discord
import sys
import json
import traceback
import io
import re
import math
import datetime
import time
import asyncio
import random
import dateutil.parser
import operator

# Init Discord client
if len(sys.argv) < 1:
        print("Usage: "+sys.argv[0]+" <DISCORD_TOKEN>")
        exit(0)

discord_token = sys.argv[1]
client = discord.Client()

#Constantes
API=[]
API.append([u"b",[u"**b**al",u"**b**eau"],"Voiced_bilabial_plosive.ogg"])
API.append([u"d̪",[u"**d**oux"],"Voiced_dental_stop.ogg"])
API.append([u"f",[u"**f**ête",u"**p**harmacie"],"Voiceless_labiodental_fricative.ogg"])
API.append([u"ɡ",[u"**g**ain",u"**gu**erre"],"Voiced_velar_plosive.ogg"])
API.append([u"k",[u"**c**abas",u"ar**ch**aï**qu**e"],"Voiceless_velar_plosive.ogg"])
API.append([u"l",[u"**l**oup"],"Alveolar_lateral_approximant.ogg"])
API.append([u"m",[u"**m**erde",u"ho**mm**e"],"Bilabial_nasal.ogg"])
API.append([u"n",[u"**n**ous",u"bo**nn**e"],"Alveolar_nasal.ogg"])
API.append([u"ŋ",[u"parki**ng**"],"Velar_nasal.ogg"])
API.append([u"ɲ",[u"a**gn**eau"],"Palatal_nasal.ogg"])
API.append([u"p",[u"**p**assé"],"Voiceless_bilabial_plosive.ogg"])
API.append([u"ʁ",[u"**r**oue",u"**rh**ume"],"Voiced_uvular_fricative.ogg"])
API.append([u"s",[u"**s**a",u"hau**ss**e",u"**sc**ie",u"**c**e",u"gar**ç**on",u"op**t**ion"],"Voiceless_alveolar_sibilant.ogg"])
API.append([u"ʃ",[u"**ch**ou",u"**sch**éma",u"**sh**ampooing"],"Voiceless_palato-alveolar_sibilant.ogg"])
API.append([u"t̪",[u"**t**out",u"**th**é"],"Pl-tom.ogg"])
API.append([u"v",[u"**v**ous",u"**w**agon"],"Voiced_labiodental_fricative.ogg"])
API.append([u"z",[u"ba**s**e",u"**z**éro"],"Voiced_alveolar_sibilant.ogg"])
API.append([u"ʒ",[u"**j**e",u"**j**au**g**e",u"pi**ge**on"],"Voiced_palato-alveolar_sibilant.ogg"])

TEST = 0
PROD = 1

#Au démarrage
@client.event
async def on_ready():
	print("* Bot "+client.user.name+" logged successfully")

	while True:
		try:
			with open('backup.json', 'r') as infile:
				data = json.load(infile)
				lastEntry = data.get("lastEntry",0)
				#lastEntry -= (lastEntry % (24*3600)) + 14*3600 + 30 * 60
				apiEntries = data.get("apiEntries",[""])

		except FileNotFoundError:
			lastEntry = 0
			apiEntries = [""]

		print(lastEntry)
		timeSinceLast = time.time() - lastEntry
		timeBetweenEntries = 3600 * 24
		sleepDuration = timeBetweenEntries-timeSinceLast
		#For      [TEST,                 PROD]
		chansId = ["613650993611145216","613360351689310218"]
		chanId = chansId[PROD]
		if sleepDuration > 0:
			await asyncio.sleep(sleepDuration)
		try:
			with open('backup.json', 'r') as infile:
				data = json.load(infile)
				apiEntries = data.get("apiEntries",[])

			if len(apiEntries) == 0:
				apiEntries = API
			entry = random.choice(apiEntries)
			#print("[rem]then :",";".join(apiEntries[0]))
			apiEntries.remove(entry)
			#print("[rem]now :",";".join(apiEntries[0]))
			msgContent="**Le caractère API du jour !**\n`/" + entry[0] + "/` se pronnonce comme dans " + " ou ".join(entry[1]) + "."
			if len(entry) >= 3:
				await client.send_file(client.get_channel(chanId),entry[2],content=msgContent)
			else:
				await client.send_message(client.get_channel(chanId),msgContent)

		except IndexError:
			#await client.send_message(client.get_channel(chanId),"Événement tout les deux jours [À venir !! — Donnez-moi des idées d’événement]")
			print("no entry")

		with open('backup.json', 'w') as outfile:
			data = {}
			lastEntry = time.time()
			data["lastEntry"] = lastEntry
			data["apiEntries"] = apiEntries
			json.dump(data, outfile)
		await asyncio.sleep(1)


@client.event
async def on_message(message):
	try:
		if not message.server:
			return
		
		if message.author.bot:
			return
		if message.content.find("!") != 0:
			return
		
		msgContent = message.content[len("!"):].strip()
		msgKeywords = msgContent.split(" ")
		if len(msgKeywords) == 0:
			return
		
		cmd = msgKeywords[0].strip()
		
		if cmd == "help":
			text = "**Commandes:**\n\n"
			text = text + "`!r [nombre]` lance [nombre] dés\n"
			await client.send_message(message.channel, text)
			return

	except:
		await client.send_message(message.channel, "Oups...")
		print(traceback.format_exc())

client.run(discord_token)

